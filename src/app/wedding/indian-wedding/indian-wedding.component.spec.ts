import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndianWeddingComponent } from './indian-wedding.component';

describe('IndianWeddingComponent', () => {
  let component: IndianWeddingComponent;
  let fixture: ComponentFixture<IndianWeddingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndianWeddingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndianWeddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
