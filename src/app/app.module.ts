import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './material.moduel';
import { AccommodationComponent } from './accommodation/accommodation.component';
import { RoomComponent } from './accommodation/room/room.component';
import { LifestyleComponent } from './lifestyle/lifestyle.component';
import { WellnessAndSpaComponent } from './lifestyle/wellness-and-spa/wellness-and-spa.component';
import { ResortActivitiesComponent } from './lifestyle/resort-activities/resort-activities.component';
import { ExperiencesComponent } from './lifestyle/experiences/experiences.component';
import { ExucutionsComponent } from './lifestyle/exucutions/exucutions.component';
import { WeddingComponent } from './wedding/wedding.component';
import { WesternWeddingComponent } from './wedding/western-wedding/western-wedding.component';
import { IndianWeddingComponent } from './wedding/indian-wedding/indian-wedding.component';
import { CelebrationComponent } from './celebration/celebration.component';
import { OfferComponent } from './offer/offer.component';
import { AuthComponent } from './auth/auth.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { FoodAndBeverageComponent } from './food-and-beverage/food-and-beverage.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { NavtabsComponent } from './navigation/navtabs/navtabs.component';

@NgModule({
  declarations: [
    AppComponent,
    AccommodationComponent,
    RoomComponent,
    LifestyleComponent,
    WellnessAndSpaComponent,
    ResortActivitiesComponent,
    ExperiencesComponent,
    ExucutionsComponent,
    WeddingComponent,
    WesternWeddingComponent,
    IndianWeddingComponent,
    CelebrationComponent,
    OfferComponent,
    AuthComponent,
    SigninComponent,
    SignupComponent,
    FoodAndBeverageComponent,
    HomeComponent,
    HeaderComponent,
    SidenavListComponent,
    NavtabsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
